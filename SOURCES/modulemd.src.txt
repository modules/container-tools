---
document: modulemd
version: 2
data:
  name: container-tools
  stream: 1.0
  summary: >-
    Stable versions of podman 1.0, buildah 1.5, skopeo 0.1, runc, conmon, CRIU,
    Udica, etc as well as dependencies such as container-selinux built and
    tested together, and supported for 24 months.
  description: >-
    Stable versions of podman 1.0 , buildah 1.5, skopeo 0.1, runc, conmon,
    CRIU, Udica, etc as well as dependencies such as container-selinux built
    and tested together. Released with RHEL 8.0 and supported for 24 months.
    During the support lifecycle, back ports of important, critical
    vulnerabilities (CVEs, RHSAs) and bug fixes (RHBAs) are provided to this
    stream, and versions do not move forward. For more information see:
    https://access.redhat.com/support/policy/updates/containertools
  license:
    module:
    - MIT
  dependencies:
  - buildrequires:
      go-toolset: [rhel8]
      golang-ecosystem: [1.0]
      platform: [el8]
    requires:
      platform: [el8]
  references:
    community: https://github.com/projectatomic
    documentation: https://projectatomic.io
    tracker: https://github.com/projectatomic
  profiles:
    common:
      rpms:
      - buildah
      - container-selinux
      - containernetworking-plugins
      - criu
      - fuse-overlayfs
      - oci-systemd-hook
      - oci-umount
      - podman
      - runc
      - skopeo
      - slirp4netns
  api:
    rpms:
    - buildah
    - container-selinux
    - containernetworking-plugins
    - containers-common
    - fuse-overlayfs
    - oci-systemd-hook
    - oci-umount
    - podman
    - podman-docker
    - runc
    - skopeo
    - slirp4netns
  buildopts:
    rpms:
      macros: |
        %_with_ignore_tests 1
  components:
    rpms:
      buildah:
        rationale: Primary component of this module
        ref: stream-container-tools-1.0-rhel-8.3.0
      container-selinux:
        rationale: Primary component of this module
        ref: stream-container-tools-1.0-rhel-8.3.0
      containernetworking-plugins:
        rationale: Primary component of this module
        ref: stream-container-tools-1.0-rhel-8.3.0
      criu:
        rationale: Primary component of this module
        ref: stream-container-tools-1.0-rhel-8.3.0
      fuse-overlayfs:
        rationale: Primary component of this module
        ref: stream-container-tools-1.0-rhel-8.3.0
      oci-systemd-hook:
        rationale: Primary component of this module
        ref: stream-container-tools-1.0-rhel-8.3.0
      oci-umount:
        rationale: Primary component of this module
        ref: stream-container-tools-1.0-rhel-8.3.0
      podman:
        rationale: Primary component of this module
        ref: stream-container-tools-1.0-rhel-8.3.0
      runc:
        rationale: Primary component of this module
        ref: stream-container-tools-1.0-rhel-8.3.0
      skopeo:
        rationale: Primary component of this module
        ref: stream-container-tools-1.0-rhel-8.3.0
      slirp4netns:
        rationale: Primary component of this module
        ref: stream-container-tools-1.0-rhel-8.3.0
...
